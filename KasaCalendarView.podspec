#
# Be sure to run `pod lib lint KasaCalendarView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = "KasaCalendarView"
s.version          = "1.2.2"
s.summary          = "Fully customizable date range picker"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = "KasaCalendarView is forked from https://github.com/Glow-Inc/GLCalendarView"

s.homepage         = "https://bitbucket.org/kasadev/kasacalendarview"
# s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
s.license          = 'MIT'
s.author           = { "leo" => "leo@glowing.com" }
s.source           = { :git => "https://ismailbozk@bitbucket.org/kasadev/kasacalendarview.git", :tag => 'v1.2.2' }

s.ios.deployment_target = '8.0'

s.source_files = "GLCalendarView/Sources/**/*.{h,m}"
s.resources = [
"GLCalendarView/Sources/**/*.{png}",
"GLCalendarView/Sources/**/*.{storyboard,xib}",
]
# s.public_header_files = 'Pod/Classes/**/*.h'
s.frameworks = 'UIKit'
# s.dependency 'AFNetworking', '~> 2.3'
end
